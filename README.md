# Diffusion Models

This repo houses my short implementation of Diffusion Models (DMs) using PyTorch Lightning. The original non-lightning DM code, unless specified, was written by me for a different project (https://git.rwth-aachen.de/diffusion-project).