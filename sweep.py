from dataloader.load import  *
from models.UnconditionalDiffusionModel import *
from models.unets import *
import torch 
from pytorch_lightning import Trainer
from models.UnconditionalDiffusionModel import *
from pytorch_lightning.loggers import WandbLogger
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint, EarlyStopping
import wandb
from datetime import datetime


#############
# Sweep:
#############

if __name__ == '__main__':

    def sweep_training():
        with wandb.init() as run:
            config = wandb.config
            device='mps'
            dataset_path = '/Users/gonzalo/pytorch_datasets/lhq_256'

            data_module = LHQDataModule(fpath=dataset_path, img_size=128,  ext=".png", batch_size=config.batch_size,  transform=True, frac=0.9)

            net = UNet_Res(attention=config.attention, channels_in=3, n_channels=64,fctr = config.fctr,time_dim=256)
            ddpm = DDPM(net=net,
                        diffusion_steps = config.diffusion_steps, 
                        out_shape = (3,256,256), 
                        noise_schedule = 'linear', 
                        beta_1 = 1e-4, 
                        beta_T = 0.02,
                        var_schedule = 'same', 
                        kl_loss = 'simplified', 
                        recon_loss = 'none',
                        # training param
                        optimizer = config.optimizer,
                        learning_rate=config.learning_rate, 
                        T_max=2532, 
                        eta_min=1e-8, 
                        decay=config.decay,
                        device = device)
            
            lr_monitor_callback = LearningRateMonitor(logging_interval='step')
            early_stop_callback = EarlyStopping(monitor='val_loss',
                                                min_delta=0.00,
                                                patience=10,
                                                verbose=False,
                                                mode='min')
            timestamp = datetime.now().strftime("%Y%m%d-%H%M%S")
            checkpoint_path = f'/Users/gonzalo/git/text-diffusion/checkpoints/{timestamp}_{run.id}'
            os.makedirs(checkpoint_path, exist_ok=True)
            checkpoint_callback = ModelCheckpoint(dirpath=checkpoint_path,
                                                  save_top_k=5,  
                                                  verbose=True,  
                                                  monitor='train_loss',  
                                                  mode='min',
                                                  save_last=True,
                                                  every_n_epochs=10)
            wandb_logger = WandbLogger(project='test-project', entity='gonzalomartingarcia0')

            trainer = pl.Trainer(max_epochs=config.max_epoch,
                                 check_val_every_n_epoch = 10,  # nr of epochs we evaluate the model
                                 devices=1,                     # nr of devices (-1: all available)
                                 # accelerator = 'mps',           # CPU, GPU, TPU, IPU, HPU, MPS
                                 # strategy = 'ddp',            # ddp, dp, ddp_spawn data distributed parallel
                                 #precision=16,                  # mixed-precision training
                                 callbacks=[checkpoint_callback, lr_monitor_callback],
                                 logger = wandb_logger)  
            trainer.fit(ddpm,data_module)

    # Sweep Configurations
    sweep_config = {
        'name' : 'First Sweep',
        'method': 'random', 
        'metric': {
        'goal': 'minimize',
        'name': 'train_loss'
        }, 
        'parameters': {
            'learning_rate': {
                'min': 0.00001,
                'max': 0.01
            },
            'optimizer': {
            'values': ['adamW', 'sgd']
            },
            'batch_size': {
                'values': [16, 32]
            },
            'max_epoch': {
                'values': [2, 4]
            },
            'decay': {
                'values': [0.9999, 0.95]
            },
            'attention': {
                'values': ['True', 'False']
            },
            'fctr': {
                'value': [1,2,4,4,8]
            },
            'diffusion_steps': {
                'values': [500, 1000]
            },
        }
    }

    # Run Sweep
    wandb_project = 'test-project'
    wandb_entity ='gonzalomartingarcia0'
    
    sweep_id = wandb.sweep(sweep=sweep_config, project=wandb_project, entity=wandb_entity)
    wandb.agent(sweep_id, function=sweep_training, count=3)