import pytorch_lightning as pl
from torch.utils.data import Dataset, DataLoader, random_split
from torchvision import transforms
import os 
from PIL import Image
import pandas as pd
import numpy as np

class LHQDataModule(pl.LightningDataModule):

    def __init__(self, fpath, img_size, batch_size, frac=0.8, ext=".png", transform=True):
        super().__init__()
        self.fpath = fpath
        self.img_size = img_size
        self.batch_size = batch_size
        self.frac = frac
        self.ext = ext
        self.transform = transform

    def setup(self, stage=None):
        # Training and validation dataloaders
        if stage == 'fit' or stage is None:
            dataset      = UnconditionalDataset(self.fpath, self.img_size, train=True, 
                                                    frac=self.frac,
                                                    ext=self.ext, transform=self.transform)
            train_length = int(len(dataset) * self.frac)
            val_length = len(dataset) - train_length
            self.train_dataset, self.val_dataset = random_split(dataset, [train_length, val_length])
        # Testing dataloader
        if stage == 'test' or stage is None:
            self.test_dataset = UnconditionalDataset(self.fpath, self.img_size, train=False, 
                                                         frac=self.frac,
                                                         ext=self.ext, transform=self.transform)

    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size=self.batch_size, shuffle=True, num_workers=4, pin_memory=True)

    def val_dataloader(self):
        return DataLoader(self.val_dataset, batch_size=self.batch_size, shuffle=False, num_workers=4, pin_memory=True)

    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size=self.batch_size, shuffle=False, num_workers=4, pin_memory=True)
    


# The Dataset class is an altered version from my project at (https://git.rwth-aachen.de/diffusion-project) and was used to train on Landscapes
# and Animal/Human Faces. Depending on the dataset, the implemented slight rotations may be undesired, since they will translate to the learned 
# data-generating distribution. Other options entail small color jittering, for example.
class UnconditionalDataset(Dataset):
    def __init__(self,fpath,img_size,train,frac =0.8,ext = ".png",transform=True ):

        ### Create DataFrame
        file_list = []
        for root, dirs, files in os.walk(fpath, topdown=False):
            for name in sorted(files):
                file_list.append(os.path.join(root, name))

        df = pd.DataFrame({"Filepath":file_list})
        self.df = df[df["Filepath"].str.endswith(ext)] 
            
        if train: 
            df_train = self.df.sample(frac=frac,random_state=7)
            self.df = df_train
        else:
            df_train = self.df.sample(frac=frac,random_state=7)
            df_test = df.drop(df_train.index)
            self.df = df_test
            
        if transform: 
            intermediate_size = 150
            theta = np.pi/4 -np.arccos(intermediate_size/(np.sqrt(2)*img_size))
            
            transform_rotate =  transforms.Compose([transforms.ToTensor(),
                                                    transforms.Resize(intermediate_size, antialias=True),
                                                    transforms.RandomRotation(theta/np.pi*180,interpolation=transforms.InterpolationMode.BILINEAR),
                                                    transforms.CenterCrop(img_size),
                                                    transforms.RandomHorizontalFlip(p=0.5), 
                                                    transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))])
            
            transform_randomcrop  =  transforms.Compose([transforms.ToTensor(),
                                                         transforms.Resize(intermediate_size, antialias=True),
                                                         transforms.RandomCrop(img_size),
                                                         transforms.RandomHorizontalFlip(p=0.5),
                                                         transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))])

            self.transform =  transforms.RandomChoice([transform_rotate,transform_randomcrop])
        else : 
            self.transform =  transforms.Compose([transforms.ToTensor(),
                                                  transforms.Resize(img_size, antialias=True)])
            
    def __len__(self):
        return len(self.df)
    
    def __getitem__(self,idx):
        path =  self.df.iloc[idx].Filepath
        img = Image.open(path)
        img_tensor = self.transform(img)
        return img_tensor,-1
          