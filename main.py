import sys
from dataloader.load import  *
from models.UnconditionalDiffusionModel import *
from models.unets import *
import torch 
from pytorch_lightning import Trainer
from models.UnconditionalDiffusionModel import *
from pytorch_lightning.loggers import WandbLogger
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint, EarlyStopping


if __name__ == '__main__':
    device = 'mps'

    # Usercentric parameters
    dataset_path = '/Users/gonzalo/pytorch_datasets/lhq_256'
    checkpoint_path = '/Users/gonzalo/git/lightning-diffusion/checkpoints'
    wandb_project = 'test-project'
    wandb_run_name = 'diffusion_3'
    wandb_entity ='gonzalomartingarcia0'

    # Init Dataloader
    data_module = LHQDataModule(fpath=dataset_path, img_size=128,  ext=".png", batch_size=32,  transform=True, frac=0.9)

    # Init UNet
    net = UNet_Res(attention=True,channels_in=3, n_channels=64,fctr = [1,2,4,4,8],time_dim=256)

    # Init Diffusion Model
    ddpm = DDPM(net=net,
                 diffusion_steps = 1000, 
                 out_shape = (3,256,256), 
                 noise_schedule = 'linear', 
                 beta_1 = 1e-4, 
                 beta_T = 0.02,
                 var_schedule = 'same', 
                 kl_loss = 'simplified', 
                 recon_loss = 'none',
                 # training param
                 learning_rate=0.001, 
                 T_max=2532, 
                 eta_min=1e-8, 
                 decay=0.9999,
                 device = device)
    

    ####################
    # Train Model:
    ####################

    # WandB Logging
    wandb_logger = WandbLogger(project=wandb_project, name=wandb_run_name, entity=wandb_entity) #,id=run_name, resume=True)

    # LR Monitor
    lr_monitor_callback = LearningRateMonitor(logging_interval='step')

    # Early Stoppage
    early_stop_callback = EarlyStopping(monitor='val_loss',
                                        min_delta=0.00,
                                        patience=10,
                                        verbose=False,
                                        mode='min')

    # Checkpoint System
    checkpoint_callback = ModelCheckpoint(dirpath=checkpoint_path,
                                          save_top_k=5,  
                                          verbose=True,  
                                          monitor='train_loss',  
                                          mode='min',
                                          save_last=True,
                                          every_n_epochs=10)
    
    # Init Trainer
    trainer = pl.Trainer(max_epochs=50,
                         check_val_every_n_epoch = 10,  # nr of epochs we evaluate the model
                         devices=1,                     # nr of devices (-1: all available)
                         # accelerator = 'mps',         # CPU, GPU, TPU, IPU, HPU, MPS
                         # strategy = 'ddp',            # ddp, dp, ddp_spawn data distributed parallel
                         # precision=16,                # mixed-precision training
                         callbacks=[checkpoint_callback, lr_monitor_callback],
                         logger = wandb_logger)
    
    # Train
    trainer.fit(ddpm, data_module)


    ####################
    # Run Model
    ####################

    # Load Model
    # checkpoint_path = "/Users/gonzalo/git/text-diffusion/checkpoints/last.ckpt"
    # ddpm = DDPM.load_from_checkpoint(checkpoint_path, net = net,diffusion_steps = 500, 
    #                                                   out_shape = (3,128,128), 
    #                                                   noise_schedule = 'linear', 
    #                                                   beta_1 = 1e-4, 
    #                                                   beta_T = 0.02,
    #                                                   var_schedule = 'same', 
    #                                                   kl_loss = 'simplified', 
    #                                                   recon_loss = 'none',
    #                                                   # training param
    #                                                   learning_rate=0.001, 
    #                                                   T_max=2532, 
    #                                                   eta_min=1e-8, 
    #                                                   decay=0.9999)

    # # Inference
    # dummy_dataloader = DataLoader(range(10), batch_size=5)
    # trainer = pl.Trainer()
    # predictions = trainer.predict(ddpm, dataloaders=dummy_dataloader)